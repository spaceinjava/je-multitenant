package com.spacein.config.multitenant;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

import com.spacein.config.TenantContext;

@Component
public class TenantSchemaResolver implements CurrentTenantIdentifierResolver{

    private String defaultTenant ="test";

    @Override
    public String resolveCurrentTenantIdentifier() {
    	
        String t =  TenantContext.getCurrentTenant();
        System.out.println("tenan resolver"+ t);
        if(t!=null){
            return t;
        } else {
            return defaultTenant;
        }
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}
