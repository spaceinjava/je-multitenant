package com.spacein.constant;

public interface MultiTenantConstants {
    String DEFAULT_TENANT_ID = "test";
    String CURRENT_TENANT_IDENTIFIER = "CURRENT_TENANT_IDENTIFIER";
    String TENANT_KEY = "tenant";
}
